# README #

This is a GroupMe bot by Brian Tung. It's pretty extensible and stuff.

### How do I get set up? ###

To add this bot to your own GroupMe chat, simply do the following:

* Create a new bot over at [GroupMe's dev page](http://dev.groupme.com)

* Change the bot ID in the code. Please don't use my IDs because it'll fail and won't be good.

* Set callback. Callback URL is 8800 + option.

### Contribution guidelines ###

* Fork

* Create PR

* I'll merge if it's appropriate

### Who do I talk to? ###

* Brian Tung: find me on campus or text me if you know my number or find me on the relevant GroupMe.

### Credits ###

* This project makes use of [GroupMe-Java-API](https://github.com/wreed12345/GroupMe-Java-API) and [ChatterBot API](https://github.com/pierredavidbelanger/chatter-bot-api).