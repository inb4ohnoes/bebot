
public class DefaultCommands {
	public static String processString(String str, String user)
	{
		if (str.contains("!source"))
		{
			System.out.println(user + " issued command: !source");
			return "Find the source for this bot at https://bitbucket.org/inb4ohnoes/bebot/overview";
		} else if (str.contains("!help"))
		{
			System.out.println(user + " issued command: !help");
			String help = "";
			
			help += "Available commands:\n";
			help += "- !k\n";
			help += "- !rekt\n";
			help += "- !dstatus\n";
			help += "- !help\n";
			help += "- !about\n";
			help += "- !source\n";
			help += "- Other things to do include asking bebot what's for breakfast, lunch, or dinner. You can also say, \"Hey Bebot\" to start a conversation and \"Shut up Bebot\" to stop it.";
			return help;
		} else if (str.contains("!about"))
		{
			System.out.println(user + " issued command: !about");
			return "Hey, I'm Bebot, a bot created by Brian Tung.\nIf you want more features, just talk to Brian and he'll probably be too lazy to implement it. " + randomEnd();
		} else
		{
			return null;
		}
	}
	
	public static String randomEnd()
	{
		int rand = (int)(Math.round(Math.random() * 5));
		if (rand == 0)
		{
			return "Good day!";
		} else if (rand == 1)
		{
			return "Now go away, or I shall taunt you a second time!";
		} else if (rand == 2)
		{
			return "What is love?";
		} else if (rand == 3)
		{
			return "Wyh u do dis?";
		} else if (rand == 4)
		{
			return "Have You Ever Considered How Can Mirrors Be Real If Our Eyes Aren't Real";
		} else if (rand == 5)
		{
			return "Desu~";
		}
		return null;
	}
}
