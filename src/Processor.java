import github.io.wreed12345.Bot;

public class Processor {

	public static String processString(Bot bot, String str, String user) throws Exception
	{
		/**
		 * //module system
		 * two types of commands
		 * commands checked in descending order for precedence
		 * 1. One off commands
		 * 2. Mode commands - changes mode of Bebot, like chatterbot
		 * just set returnStr to whichever static method and check if it's null before every process
		 * i was going to have a interface that you could implement so everything could have the required methods
		 * except static methods in interfaces can only be done in java 8 and i'm not gonna deal with that shit
		 * right now
		 */
		

		String returnStr = null;

		if (returnStr == null) returnStr = DefaultCommands.processString(str, user);

		if (returnStr == null) returnStr = MiscCommands.processString(str, user);
		
		if (returnStr == null) returnStr = FoodCommands.processString(str, user);

		if (returnStr == null) returnStr = StarbucksCounter.processString(str, user);
		
		if (returnStr == null) returnStr = BebotChat.processString(str, user);
		
		return returnStr;
	}
}
