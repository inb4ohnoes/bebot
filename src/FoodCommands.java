import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class FoodCommands {
	
	public static String processString(String str, String user) throws MalformedURLException, IOException
	{
		if (str.contains("bebot") && (str.contains("what") && (str.contains("dinner") || str.contains("lunch") || str.contains("breakfast"))))
		{
			//TODO: other dining courts
			FoodGrabber grabber = new FoodGrabber(new URL("http://www.housing.purdue.edu/Menus/ERHT/"));
			Menu menu = grabber.getFood();
			if (str.contains("breakfast"))
			{
				System.out.println(user + " asked what's for breakfast");

				if (!menu.getMeals()[2].getIsServing()) {
					return "Who even eats breakfast? Nothing's serving anyways so...";
				}
				StringBuilder out = new StringBuilder("Here's whats for breakfast at Earhart:\n");
				for (MenuItem item : menu.getMeals()[0].getMenuItems()) {
					out.append(item.toString() + "\n");
				}
				return out.toString();
			} else if (str.contains("lunch"))
			{
				System.out.println(user + " asked what's for lunch");

				if (!menu.getMeals()[2].getIsServing()) {
					return "Here's what's for lunch... wait nothing's serving, jk.";
				}
				StringBuilder out = new StringBuilder("Here's whats for lunch at Earhart:\n");
				for (MenuItem item : menu.getMeals()[1].getMenuItems()) {
					out.append(item.toString() + "\n");
				}
				return out.toString();
			} else
			{
				//dinner
				System.out.println(user + " asked what's for dinner");

				if (!menu.getMeals()[2].getIsServing()) {
					return "Looks like no dinner for you!";
				}
				StringBuilder out = new StringBuilder("Here's whats for dinner at Earhart:\n");
				for (MenuItem item : menu.getMeals()[2].getMenuItems()) {
					out.append(item.toString() + "\n");
				}
				return out.toString();
			}
		}
		return null;
	}
}
