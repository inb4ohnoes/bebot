
public class BebotChat
{

	private static boolean isChatting = false;

	private static ChatterBotFactory factory = new ChatterBotFactory();

	private static ChatterBotSession chatterbotSession;

	public static String processString(String str, String user) throws Exception
	{
		if (str.contains("hey bebot"))
		{
			isChatting = true;
			//init chatterbot
			ChatterBot chatterbot = factory.create(ChatterBotType.CLEVERBOT);
			chatterbotSession = chatterbot.createSession();
			System.out.println("Started chatting");
			return "Whatsup " + user;
		}
		
		if (isChatting && str.contains("shut up bebot"))
		{
			isChatting = false;
			chatterbotSession = null;
			System.out.println("Stopped chatting");
			return "Ok :(";
		} else if (isChatting)
		{
			return chatterbotSession.think(str);
		}
		
		return null;
	}
}
