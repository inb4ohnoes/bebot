import com.sun.net.httpserver.HttpExchange;


public class StarbucksCounter {
	
	private static int numberOfStarbucks;
	
	public static String processString(String str, String user)
	{
		if (str.contains("!starbucks"))
		{
			System.out.println(user + " issued command: !bstar");
			return "Brandon owes Brian " + getNumber() + " Starbucks'";
		}
		return null;
	}
	
	public static String processCommand(HttpExchange t)
	{
		int shouldSendCount = Integer.parseInt((String)t.getRequestHeaders().getFirst("shouldSendCount"));
		if (shouldSendCount == 0)
		{
			setNumber(Integer.parseInt((String)t.getRequestHeaders().getFirst("count")));
			return "" + getNumber();
		} else
		{
			return "" + getNumber();
		}
	}
	
	public static void setNumber(int num)
	{
		numberOfStarbucks = num;
	}
	
	public static int getNumber()
	{
		return numberOfStarbucks;
	}
}
