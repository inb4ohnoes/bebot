
public class MiscCommands {
	public static String processString(String str, String user)
	{
		if (str.contains("!k"))
		{
			System.out.println(user + " issued command: !k");
			String k = "";
			k += "KKKK                        KKKK\n";
			k += "KKKK                    KKKK\n";
			k += "KKKK                KKKK\n";
			k += "KKKK            KKKK\n";
			k += "KKKK        KKKK\n";
			k += "KKKK    KKKK\n";
			k += "KKKKKKKKK\n";
			k += "KKKKKKK\n";
			k += "KKKKKKKKK\n";
			k += "KKKK    KKKK\n";
			k += "KKKK        KKKK\n";
			k += "KKKK            KKKK\n";
			k += "KKKK                KKKK\n";
			k += "KKKK                    KKKK\n";
			k += "KKKK                       KKKK";
			return k;
		} else if (str.contains("!dstatus"))
		{
			System.out.println(user + " issued command: !dstatus");
			return "Dominick's not in his room.";
		} else if (user.toLowerCase().contains("zach thompson"))
		{
			System.out.println(user + " said something");
			return randomZach();
		} else if (user.toLowerCase().contains("dominick lee"))
		{
			System.out.println(user + " issued command: dominick said something");
			if ((int)(Math.round(Math.random() * 5)) == 1) return "Ok Dominick.";
		} else if (str.contains("!rekt"))
		{
			System.out.println(user + " issued command: !rekt");
			int mark = (int)Math.round(Math.random() * 10);
			String meter = "Rekt meter for previous message: \n";
			meter += "0 <";
			for (int i = 0; i < 11; i++)
			{
				if (i == mark) meter += "|";
				meter += "-";
			}
			meter += "> rekt";
			return meter;
		}
		return null;
	}
	
	public static String randomZach()
	{
		int rand = (int)Math.round(Math.random() * 2);
		if (rand == 0)
		{
			return "Zach pls.";
		} else if (rand == 1)
		{
			return "I love you Zach <3";
		} else if (rand == 2)
		{
			return "Lookin' zesty today Zach";
		}
		return null;
	}
}
